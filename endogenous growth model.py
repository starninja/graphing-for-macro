import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np

data = pd.DataFrame([0, 1, 2, 3, 4, 5, 6, 7, 8])

data.columns = ['g_A(t)']
theta = 0.5

print(data.dtypes)

data['g_A(t)'] = data['g_A(t)'].astype(float)
data['g_A(t)_sq'] = data['g_A(t)']*data['g_A(t)']
data['g_A(t)_dot'] = 2 * data['g_A(t)'] + (theta-1) * (data['g_A(t)_sq'])
data['g_A(t)_dot, theta = 1'] = 2 * data['g_A(t)'] + (1-1) * (data['g_A(t)_sq'])
data['g_A(t)_dot, increased gamma'] = 3 * data['g_A(t)'] + (theta-1) * (data['g_A(t)_sq'])

#data.drop(['k'], inplace=True)

data[['g_A(t)_dot', 'g_A(t)_dot, increased gamma', 'g_A(t)_dot, theta = 1']].plot(legend=True)
#plt.plot([0, 2], [0.25, 0.25], 'k-', lw=1) #Line 1 start
#plt.plot([2, 2], [0, 0.25], 'k-', lw=1) #Line 1
#plt.plot([0, 3.75], [0.485, 0.485], 'k-', lw=1) #Line 2 start
#plt.plot([3.75, 3.75], [0, 0.485], 'k-', lw=1) #Line 2 end

plt.show()
