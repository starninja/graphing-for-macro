import pandas as pd
import matplotlib.pyplot as plt
import math
import numpy as np

data = pd.DataFrame([0,0.5, 1,1.5, 2,2.5,3,3.5,4, 4.5,5, 5.5,6, 6.5,7, 7.5,8, 8.5,9, 9.5,10])

data.columns = ['k']

print(data.dtypes)

data['k'] = data['k'].astype(float) 
data['f(k)'] = np.sqrt(data['k'])
data['Breakeven Investment'] = 0.25*data['k']
data['Actual Investment'] = 0.25*data['f(k)']
data['Actual Investment (new savings rate)'] = 0.35*data['f(k)']

#data.drop(['k'], inplace=True)

data[['f(k)','Breakeven Investment','Actual Investment', 'Actual Investment (new savings rate)']].plot(legend=True)
plt.plot([0, 2], [0.25, 0.25], 'k-', lw=1) #Line 1 start
plt.plot([2, 2], [0, 0.25], 'k-', lw=1) #Line 1
plt.plot([0, 3.75], [0.485, 0.485], 'k-', lw=1) #Line 2 start
plt.plot([3.75, 3.75], [0, 0.485], 'k-', lw=1) #Line 2 end

plt.show()
